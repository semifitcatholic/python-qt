
import sys
import numpy as np
from PyQt6 import QtCore, QtGui, QtWidgets
from PyQt6.QtCore import Qt

MAX_BUTTONS = 5

class TableModel(QtCore.QAbstractTableModel):
    def __init__(self, data):
        super(TableModel, self).__init__()
        self._data = data
    
    def data(self, index, role):
        if role == Qt.ItemDataRole.DisplayRole:
            return self._data[index.row()][index.column()]

    def rowCount(self, index):
        return len(self._data)

    def columnCount(self, index):
        return len(self._data[0])

    def change_data(self, data):
        self._data = data

class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.lines = []
        self.btns = []
        self.setWindowTitle("My Fitness App")
        self.table = QtWidgets.QTableView()

        data = [
            [1,2,3],
            [4,5,6],
            [4,5,6],
            [4,5,6],
            [4,5,6],
            [4,5,6],
            [4,5,6],
            [4,5,6],
            [7,8,9],
            [10,11,12],
            [13,14,15],
        ]
        self.model = TableModel(data)
        self.table.setModel(self.model)

        layout = QtWidgets.QGridLayout()

        self.setup_btns()
        for i in range(0,MAX_BUTTONS):
            layout.addWidget(self.lines[i], i, 0)
            layout.addWidget(self.btns[i], i,1)
        
        self.table.horizontalHeader().setStretchLastSection(True)
        # self.table.horizontalHeader().setSectionResizeMode(
        #     QHeaderView.Stretch)
        hbox = QtWidgets.QHBoxLayout()
        hbox.addLayout(layout)
        hbox.addWidget(self.table)
    

        widget = QtWidgets.QWidget()
        widget.setLayout(hbox)
        self.setCentralWidget(widget)

    def setup_btns(self):
        for i in range(0, MAX_BUTTONS):
            line = QtWidgets.QLineEdit("Blank")
            self.lines.append(line)

            btn = QtWidgets.QPushButton("Button {}".format(i))
            btn.pressed.connect(self.change_data)
            self.btns.append(btn)
    

    def change_data(self):
        i = np.random.randint(0,100)
        data = [
            [i+1,i+2,i+3],
            [i+4,i+5,i+6],
            [i+7,i+8,i+9],
            [i+10,i+11,i+12],
            [i+10,i+11,i+12],
            [i+10,i+11,i+12],
            [i+10,i+11,i+12],
            [i+13,i+14,i+15],
        ]
        self.model.change_data(data) 
        self.model.layoutChanged.emit()
        self.adjustSize()



if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    mainwin = MainWindow()
    mainwin.show()
    app.exec()