from numpy import NaN
import pandas as pd


# data = {'Date':[], 
#         'BW (lbs)':[],
#         'Waist length (in)':[],
#         'Pike (in from floor)':[],
#         'Squat (lbs 5 reps)':[],
#         'Deadlift (lbs 5 reps)':[],
#         'Bench (lbs 5 reps)':[],
#         'PullUp (lbs 5 reps)':[],
#         'Rack Pull (lbs 5 reps)':[],
#         'HS PushUps (reps)':[],
#         }
data = [['0','0','0','0','0','0','0','0','0','0',]]
df = pd.DataFrame(data, columns= [
        'Date',
        'BW (lbs)',
        'Waist length (in)',
        'Pike (in from floor)',
        'Squat (lbs 5 reps)',
        'Deadlift (lbs 5 reps)',
        'Bench (lbs 5 reps)',
        'PullUp (lbs 5 reps)',
        'Rack Pull (lbs 5 reps)',
        'HS PushUps (reps)',
       ],
       #index=[' '],
       
)


df.to_csv("fitnessdata.csv")