
from cmath import nan
from numpy import NaN
import PDTable
import os, sys
import pandas as pd

from PyQt5 import QtCore, uic, QtWidgets


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("mainwindow.ui", self)
        self.dateEdit.setDate(QtCore.QDate.currentDate())

        # data = pd.DataFrame([[1,2,3], [4,5,6], [7,8,9], [10,11,12], [13,14,15],], 
        #                     columns= ["A", "B", "C"],
        #                     index= ["Row 1", "Row 2", "Row 3", "Row 4", "Row 5"], 
        #                     )
        self.clickedSave = True
        self.data = pd.read_csv("fitnessdata.csv")
        print(self.data['Date'][0])
        if self.data['Date'][0] == 0:
            print('switching to false')
            self.clickedSave = False

        self.dataModel = PDTable.TableModel(self.data)
        self.dataTable.setModel(self.dataModel)

        self.SaveButton.pressed.connect(self.SaveDataLine)
        self.PrintButton.pressed.connect(self.PrintData)
        # print("Checked status ", self.SaveButton.isChecked())

    def SaveDataLine(self):
        new_row = pd.DataFrame([[
                self.dateEdit.text(), 
                self.WeightLineEdit.text(),
                self.WaistLineEdit.text(),
                self.PikeLineEdit.text(),
                self.SquatLineEdit.text(),
                self.DeadLiftLineEdit.text(),
                self.BenchLineEdit.text(),
                self.PullUpLineEdit.text(),
                self.RackPullLineEdit.text(),
                self.HSLineEdit.text(),
                ]], columns=[
                        'Date',
                        'BW (lbs)',
                        'Waist length (in)',
                        'Pike (in from floor)',
                        'Squat (lbs 5 reps)',
                        'Deadlift (lbs 5 reps)',
                        'Bench (lbs 5 reps)',
                        'PullUp (lbs 5 reps)',
                        'Rack Pull (lbs 5 reps)',
                        'HS PushUps (reps)',
                   ],
                  # index= [' '],
        )

        # print("Checked status ", self.SaveButton.isChecked())
        if self.clickedSave == True: #self.SaveButton.isChecked() == True:
            print("concatanating")
            self.data = pd.concat([self.data, new_row], ignore_index=True).reset_index(drop=True)

        if  self.clickedSave == False:
            self.data = new_row
            self.clickedSave = True

        self.dataModel._data = self.data
        self.dataModel.layoutChanged.emit()

    def PrintData(self):
        print(self.data)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow()
    window.show()
    app.exec()