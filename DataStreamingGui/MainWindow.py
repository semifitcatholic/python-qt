from PyQt6.QtGui import QIcon
from PyQt6.QtCore import QSize, Qt
from PyQt6.QtWidgets import (QApplication, QMainWindow, QGridLayout, 
    QPushButton, QLabel, QVBoxLayout, QWidget, QHBoxLayout)
import numpy as np
import sys
import pyqtgraph as pg

# Subclass QMainWindow to customize your application''s main window
class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Data Display")
        self.setGeometry(100,100,600, 500)
        self.setWindowIcon(QIcon("DataStreamingGui/Ball+Logo.png"))



        layout = QVBoxLayout()


        buttons = [QPushButton("Open Display")]

        self.SetupUI()
        self.show()

        # set the central widget of the Window.
        # self.setCentralWidget(button)
    
    def SetupUI(self):
        widget = QWidget()


        pg.setConfigOptions(antialias=True)

        imv = pg.ImageView()

        img = pg.gaussianFilter(np.random.normal(
            size=(200,200)), (5,5)) * 20 + 100
        img = img[np.newaxis, :, :]

        decay = np.exp(-np.linspace(0, 0.3, 100))[:, np.newaxis, np.newaxis]

        data = np.random.normal(size=(100, 200, 200))
        data += img * decay
        data += 2 
# adding time-varying signal
        sig = np.zeros(data.shape[0])
        sig[30:] += np.exp(-np.linspace(1, 10, 70))
        sig[40:] += np.exp(-np.linspace(1, 10, 60))
        sig[70:] += np.exp(-np.linspace(1, 10, 30))
        sig = sig[:, np.newaxis, np.newaxis] * 3
        data[:, 50:60, 30:40] +=sig

        imv.setImage(data, xvals=np.linspace(1., 2., data.shape[0]))

        colors = [
            (0,0,0),
            (45,5,61),
            (84,42,55),
            (150,87,60),
            (208,171,141),
            (255,255,255)
        ]

        cmap = pg.ColorMap(pos=np.linspace(0.0, 1.0, 6), color=colors)

        imv.setColorMap(cmap)

        layout = QGridLayout()

        label = QLabel("Async Image View")
        label.setMinimumWidth(130)
        label.setWordWrap(True)
        label.setFixedWidth(130)

        widget.setLayout(layout)

        layout.addWidget(imv, 0, 1, 3, 1)

        self.setCentralWidget(widget)

        value = imv.getHistogramWidget()

        label.setText("Histogram Widget : " + str(value))
    


app = QApplication(sys.argv)
window = MainWindow()

sys.exit(app.exec())

