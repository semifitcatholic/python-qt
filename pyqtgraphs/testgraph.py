
from PyQt6.QtWidgets import (QApplication, QMainWindow, QGridLayout, 
     QPushButton, QLabel, QVBoxLayout, QWidget, QHBoxLayout)
import pyqtgraph as pg
import numpy as np
import sys

time = np.linspace(0,10,1000)
data = 5*np.random.rand(1000,1000)
app = QApplication(sys.argv)
imv = pg.ImageView()
imv.show()
imv.setImage(data)


sys.exit(app.exec())